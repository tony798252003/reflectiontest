﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Assembly[] ass = AppDomain.CurrentDomain.GetAssemblies();
            Person p = new Person();
            p.TestAssembly();
            p.CreatePersonObject();
            p.TypeTest1();
        }
    }




    class Person
    {
        public String Name { get; set; }
        public int Age { get; set; }
        private int id;

        public void TestAssembly()
        {
            Assembly ass = this.GetType().Assembly;
            Type[] types = ass.GetTypes();
            Type currentType = ass.GetType();
            //根据类的full name，获取在内存中的类型对象  
            Type typeByFullName = ass.GetType("ReflectionTest.Person");

            Type type = this.GetType();
            MethodInfo[] methods = this.GetType().GetMethods();
            this.GetType().GetMembers();
            this.GetType().GetMember("Name");
            FieldInfo field = type.GetField("id");
            PropertyInfo prop = type.GetProperty("Name");


        }

        public void CreatePersonObject()
        {
            Type type = this.GetType();
            //使用Person类的Type对象，创建一个Person对象
            Person p = Activator.CreateInstance(type) as Person;
            Person p1 = Activator.CreateInstance<Person>();

            //使用Person类的Name属性对象，为p的Name属性赋值
            PropertyInfo prop = type.GetProperty("Name");
            prop.SetValue(p1, "toto");
            Console.WriteLine(p1.Name);

            //使用Person类的Sayhi方法对象，调用p1的Sayhi方法
            MethodInfo method = type.GetMethod("Sayhi");
            method.Invoke(p1, null);
            MethodInfo method1 = type.GetMethod("ShowNumber");
            object[] arrParams = {2};
            method1.Invoke(p1, arrParams);
            MethodInfo method2 = type.GetMethod("GetString");
            String retStr = method2.Invoke(p1, null).ToString();
            Console.WriteLine(retStr);
        }

        public void Sayhi()
        {
            Console.WriteLine("Hiiiiii");
        }

        public void ShowNumber(int no)
        {
            Console.WriteLine(no);
        }

        public String GetString()
        {
            return "Hello";

        }

        public void TypeTest1()
        {
            Person p = new Person { Name = "toto", Age = 5 };
            Type tPerson = p.GetType();
            PropertyInfo[] props = tPerson.GetProperties();
            StringBuilder builder = new StringBuilder(30);
            foreach (PropertyInfo prop in props)
            {
                builder.Append(prop.Name + "=" + prop.GetValue(p) + "\n");
            }

            builder.Append("------------------------------- \n");
            FieldInfo[] fields = tPerson.GetFields();
            foreach (FieldInfo f in fields)
            {
                builder.Append(f.Name + "=" + f.GetValue(p) + "\n");
            }
            Console.WriteLine(builder);

        }
    }
}